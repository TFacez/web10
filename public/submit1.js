$( document ).ready(function() {
	$('#form').validate({
	rules: {
        name: {
            required:false,
            minlength:2,
        },
		phone: {
            required: true,
        },
        region:{
            required: true,
        }
    },
    messages: {
        name:{
            minlength:"В имени должно быть более 2х символов",
        },
		phone: {
            required: "",
        },
		region: {
			required: "",
		},
    },
    submitHandler: function(){
         sendAjaxForm('form', 'https://api.slapform.com/ne100800@mail.com');
		 return false; 
    }
	});
 function sendAjaxForm(feedback, url) {
					$.ajax({
						url:     url,
						type:     "POST",
						data: $("#"+feedback).serialize(),
						success: function(response) { 
							$(this).find("input").val("");
                            alert("Спасибо за заявку! Скоро мы с Вами свяжемся!");
                            $('#form').trigger("reset");
						},
						error: function(response) { 
                            alert('Ой, какая-то ошибка!');
						}
					});
                };
})